import React from 'react';
import logo from './assets/stable-logo.svg';
import './stylesheets/scss/App.scss';
import data from './data.json';
import {
  Navbar,
  NavbarBrand,
  Media,
  Container, Row, Col,
  Button,
} from 'reactstrap';

import Mail from './mail/Mail.js';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      mail: [],
      numMail: data.length,
      page: 0,
    };
  } 

  componentDidMount() {
    var end = this.state.numMail > 6 ? 6: this.state.numMail;
    this.setState({ mail: data.slice(0,end) });
  }
  
  increment = () => {
    const p = this.state.page + 1;
    this.setState({ page: p });
    var start = p * 6;
    var end = 0;
    if ( (start + 6) > this.state.numMail ) {
      end = this.state.numMail;
    }
    else {
      end = start + 6;
    }
    this.setState({ mail: data.slice(start,end) });
  } 

  decrement = () => {
    const p = this.state.page - 1;
    this.setState({ page: p });
    var start = p * 6;
    var end = start + 6;
    this.setState({ mail: data.slice(start,end) });
  }

  render() {
    return(
      <div className="App">
        <Navbar className="py-0">
          <NavbarBrand href="/" className="py-0 mx-0">
            <Media 
              left 
              object 
              src={logo} 
              style={{ 
                minHeight: 51, minWidth: 180,
                marginLeft: 20, marginTop: 15
              }} 
              alt="stable-logo"
            />
          </NavbarBrand>
        </Navbar>
        <hr className="mt-1"/>
        <Container 
          style={{ padding: '5vh 10vw', alignItems: 'center' }}
        >
          <Row>
            <Col>
              <p id="all-mail">All Mail</p>
            </Col>
          </Row>
          <Row className="pb-4">
            <Col id="all-mail-subtext">
              Here are all of the pieces of mail you've received at your Stable address.
            </Col>
          </Row>
          <Row>
            {
              this.state.mail.map((object, i) => {
                return (
                  <Col className="mb-3 nopadding" xs="12" sm="6" md="4" lg="4" xl="4" key={object.id} role={object.id}>
                    <Mail role={object.id} data={object}/>
                  </Col>
                );
              })
            }
          </Row>
          <Row>
            <Col style={{ marginTop: 'auto', marginBottom: 'auto' }}>
                <span id="num-mail">
                  <b>{this.state.numMail}</b> mail items
                </span>
            </Col>
            <Col sm={1} md={4} style={{ textAlign: 'end', marginTop: 'auto', marginBottom: 'auto' }}>
              <Button 
                className="button" 
                color="secondary" 
                outline
                disabled={ this.state.page === 0 }
                onClick={this.decrement}
                role="previous"
              >
                Previous
              </Button>
              <Button 
                className="button" 
                color="secondary" 
                outline
                onClick={this.increment}
                disabled={ ((this.state.page + 1) * 6 ) >= this.state.numMail }
                role="next"
              >
                Next
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}