import React from 'react';
import {
  Card, CardImg, CardBody,
  CardTitle, CardFooter,
  Row, Col
} from 'reactstrap';
import moment from 'moment';
import company from '../assets/company.svg';
import recipient from '../assets/recipient.svg';
import scan from '../assets/scan.svg';
import forward from '../assets/forward.svg';
import shred from '../assets/shred.svg';
import processing from '../assets/processing.svg';
import '../stylesheets/scss/Mail.scss';

export default class Mail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.data.id,
      businessRecipient: props.data.businessRecipient,
      from: props.data.from,
      image: props.data.imageUrl,
      timestamp: props.data.timestamp,

      individualRecipient: props.data.individualRecipient ? props.data.individualRecipient : null,
      forward: props.data.forward ? props.data.forward.status: null,
      scan: props.data.scan ? props.data.scan.status : null,
      shred: props.data.shred ? props.data.shred.status : null,
    }
  }

  getIcon = (icon, val) => {
    if (val === 'processing') return processing;
    else return icon;
  }

  render() {
    return (
      <Card style={{ width:"100%", height:"100%" }}>
        <CardImg 
          top
          style={{ 
            width: "100%", height: "125px",
            paddingLeft: '22px', paddingRight: '22px',
            paddingTop: '3px', paddingBottom: '0'
          }}
          src={this.state.image}
          alt="mail-img"
        />
        <hr className="mb-0"/>
        <CardBody className="py-2 px-1">
          <CardTitle className="mb-0">
            <p id="mail-from">{this.state.from}</p>
          </CardTitle>
            { this.state.businessRecipient &&
              <Row>
                <Col>
                  <span className="mail-card-span">
                    <img src={company} alt="company" className="icon"/>
                    {this.state.businessRecipient}
                  </span>
                </Col>
              </Row>
            }
            { this.state.individualRecipient &&      
              <Row>
                <Col>
                  <span className="mail-card-span">
                    <img 
                      src={this.getIcon(recipient, this.state.individualRecipient)} 
                      alt="recipient"
                      className="icon"
                    />
                    {this.state.individualRecipient}
                  </span>
                </Col>
              </Row>
            }
            { this.state.scan &&
              <Row>
                <Col>
                  <span className="mail-card-span">
                    <img 
                      src={this.getIcon(scan, this.state.scan)} 
                      alt="scan"
                      className="icon"
                    />
                    {this.state.scan === 'completed' ?
                      'Scanned' : 'Scan processing'
                    }
                  </span>
                </Col>
              </Row>
            }
            { this.state.forward &&
              <Row>
                <Col>
                  <span className="mail-card-span">
                    <img
                      src={this.getIcon(forward, this.state.forward)} 
                      alt="forward"
                      className="icon"
                    />
                    {this.state.forward === 'completed' ?
                      'Forwarded' : 'Forward processing'
                    }
                  </span>
                </Col>
              </Row>
            }
            { this.state.shred && 
              <Row>
                <Col>
                  <span className="mail-card-span">
                    <img 
                      src={this.getIcon(shred, this.state.shred)} 
                      alt="shred"
                      className="icon"
                    />
                    {this.state.shred === 'completed' ?
                      'Shredded' : 'Shred processing'
                    }
                  </span>
                </Col>
              </Row>
            }
        </CardBody>
        <CardFooter style={{ backgroundColor: 'transparent', textAlign: 'end' }}>
          <span id="mail-footer"> {moment.utc(this.state.timestamp).format('LL')} </span>
        </CardFooter>
      </Card>

    );
  }

}