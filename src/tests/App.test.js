import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import App from '../App';
import data from '../data.json';

beforeEach(() => render(<App />))

test('renders initial data', () => {
  expect(screen.getByAltText('stable-logo'));
  expect(screen.getByRole(data[0].id));
  expect(screen.getByRole('next'));
  expect(screen.getByRole('previous')).toBeDisabled();
});

test('renders correct data after navigation', async() => {
  fireEvent.click(screen.getByRole('next'));
  await waitFor(() => {
    expect(screen.getByRole('next')).toBeDisabled();
    expect(screen.getByRole(data[data.length-1].id));
  }, {
    timeout: 2500,
  });

  fireEvent.click(screen.getByRole('previous'));
  await waitFor(() => {
    expect(screen.getByRole('previous')).toBeDisabled();
    expect(screen.getByRole(data[0].id));
  }, {
    timeout: 2500,
  });

});
